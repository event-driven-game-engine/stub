#include "engine.h"
//#include <SDL2/SDL.h>
#define SDL_main main
int main ()
{
    try
    {
        Engine engine;
        return 0;
    }
    catch (EngineError problem)
    {
        fprintf(stderr,"Engine exception: %s\n",problem.what());
        return 1;
    }
}
